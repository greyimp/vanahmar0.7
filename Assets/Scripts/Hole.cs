﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour {
	ParticleSystem ps;
	public bool active;
	public float maxSize = 12.6f;
	public float lerpDownTimer = 0.4f;
	CapsuleCollider capsuleCollider, particleCapsule;
	Coroutine lerp;
	// Use this for initialization
	void Start() {
		particleCapsule = transform.Find("FlameSpew").GetComponent<CapsuleCollider>();
		ps = transform.Find("FlameSpew").GetComponent<ParticleSystem>();
		capsuleCollider = GetComponent<CapsuleCollider>();
	}
	public void Reset() {
		if (lerp != null) {
			StopCoroutine(lerp);
			lerp = null;
		}
		lerp = StartCoroutine(LerpDown(lerpDownTimer, false));
		capsuleCollider.height = 0.15f * maxSize;
		//ps.startSpeed = map((capsuleCollider.height / maxSize), 0.15f, 1.0f, 2.0f, 75f);
	}
	public void SetValue(float v, bool ac) {
		if (lerp != null) {
			StopCoroutine(lerp);
			lerp = null;
		}
		ps.Stop();
		ps.Play();

		active = ac;
		lerp = StartCoroutine(LerpDown(lerpDownTimer, true));
		capsuleCollider.height = v * maxSize;
		particleCapsule.height = v * maxSize;
		ps.startSpeed = map(v, 0.15f, 1.0f, 2.0f, 75f);
	}
	IEnumerator LerpDown(float wait, bool withphysics) {
		float step = 0.2f;
		yield return new WaitForSeconds(wait);
		while (particleCapsule.height > 0.15f * maxSize) {

			if (withphysics)
				capsuleCollider.height = Mathf.MoveTowards(capsuleCollider.height, 0.15f * maxSize, step);

			particleCapsule.height = Mathf.MoveTowards(particleCapsule.height, 0.15f * maxSize, step += 0.1f);
			ps.startSpeed = map((particleCapsule.height / maxSize), 0.15f, 1.0f, 2.0f, 75f);
			yield return null;
		}
		if (active) {
			GridCreator.instance.canBeReset = true;
		}
		active = false;
		lerp = null;
	}

	private float map(float s, float a1, float a2, float b1, float b2) {
		return Mathf.Clamp(b1 + (s - a1) * (b2 - b1) / (a2 - a1), b1, b2);
	}
}
