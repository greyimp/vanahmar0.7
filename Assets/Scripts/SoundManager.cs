﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour
{
	public static SoundManager instance;
	// The Mute Bool.
	bool mute;
	// The music volume.
	[Range(0, 1)]
	public float musicVolume;
	// The sound volume.
	[Range(0, 1)]
	public float soundVolume;
	public float LerpSpeed = 1f;
	// List Of One Shot Sounds.
	public List<SoundsItem> ListOfSounds = new List<SoundsItem>();
	// List of current AudioSource
	Dictionary<string, AudioSource> ListOfAudioSource = new Dictionary<string, AudioSource>();
	void Awake()
	{
		instance = this;
		ListOfAudioSource.Add("OneShotAudio", gameObject.AddComponent<AudioSource>());
	}

	void Start()
	{
		//ChangeMusicVolume(PlayerPrefs.GetFloat("Music", 0.5f));
		//ChangeSoundVolume(PlayerPrefs.GetFloat("Sound", 1));
	}

	void Update()
	{
		//ChangeMusicVolume(musicVolume);
		//ChangeSoundVolume(soundVolume);
	}

	public void ChangeMusicVolume(float v)
	{
		musicVolume = v;
		PlayerPrefs.SetFloat("Music", v);
		AudioSource[] totalAudios = GetComponents<AudioSource>();
		for (int i = 0; i < totalAudios.Length; i++)
		{
			if (totalAudios[i].loop)
				totalAudios[i].volume = musicVolume;
		}
	}

	public void ChangeSoundVolume(float v)
	{
		soundVolume = v;
		PlayerPrefs.SetFloat("Sound", v);
		AudioSource[] totalAudios = GetComponents<AudioSource>();
		for (int i = 0; i < totalAudios.Length; i++)
		{
			if (!totalAudios[i].loop)
				totalAudios[i].volume = soundVolume;
		}
	}
	public void MuteGame()
	{
		mute = !mute;
		AudioSource[] totalAudios = GetComponents<AudioSource>();
		for (int i = 0; i < totalAudios.Length; i++)
		{
			totalAudios[i].mute = mute;
		}
	}

	public void LoopingPitch(string id, float pitch)
	{
		AudioSource audioS;
		if (!mute)
		{
			if (ListOfAudioSource.TryGetValue(id + "Loop", out audioS))
			{
				audioS.pitch = pitch;
			}
			else
			{
				audioS = gameObject.AddComponent<AudioSource>();
				for (int i = 0; i < ListOfSounds.Count; i++)
				{
					if (id == ListOfSounds[i].ID)
					{
						audioS.pitch = pitch;
						break;
					}
				}
			}
		}
	}

	public void NoteSound(string id, float time, float pitch)
	{
		for (int i = 0; i < ListOfSounds.Count; i++)
		{
			if (id == ListOfSounds[i].ID)
			{
				AudioSource audioS = gameObject.AddComponent<AudioSource>();
				audioS.volume = 2f;
				audioS.pitch = pitch;
				//audioS.priority = 0;
				audioS.clip = ListOfSounds[i].GetRandomClip();
				//StartCoroutine(LerpSoundUp(audioS, 1));
				StartCoroutine(timeToStop(audioS, time));
			}
		}
	}

	IEnumerator timeToStop(AudioSource audioS, float time)
	{
		audioS.Play();
		yield return new WaitForSeconds(time);
		while (audioS.volume > 0)
		{
			audioS.volume -= LerpSpeed / 10;
		}
		audioS.Stop();
		Destroy(audioS);
	}

	public void OneShotSound(string id)
	{
		AudioSource audioS;
		if (!mute)
		{
			for (int i = 0; i < ListOfSounds.Count; i++)
			{
				if (id == ListOfSounds[i].ID)
				{
					if (ListOfAudioSource.TryGetValue("OneShotAudio", out audioS))
					{
						//StartCoroutine(LerpPitch(audioS, endpitch));
						audioS.PlayOneShot(ListOfSounds[i].GetRandomClip());
					}
					break;
				}
			}
		}
	}
	public IEnumerator LerpPitch(AudioSource audioS, float p)
	{
		while (audioS.pitch != p)
		{
			audioS.pitch = Mathf.Lerp(audioS.pitch, p, LerpSpeed / 10);
			yield return null;
		}
	}
	public IEnumerator LerpSoundDown(AudioSource audioS)
	{
		while (audioS.volume > 0)
		{
			audioS.volume -= LerpSpeed / 10;
			yield return null;
		}
		audioS.Stop();
	}
	public IEnumerator LerpSoundUp(AudioSource audioS, float v)
	{
		audioS.volume = 0;
		audioS.Play();
		while (audioS.volume < v)
		{
			audioS.volume += LerpSpeed / 10;
			yield return null;
		}
	}
	public void LoopingSound(string id, float pitch = 1,bool off = false)
	{
		AudioSource audioS;
		if (!mute)
		{
			if (ListOfAudioSource.TryGetValue(id + "Loop", out audioS))
			{
				if (off) { StartCoroutine(LerpSoundDown(audioS)); } else { StartCoroutine(LerpSoundUp(audioS, musicVolume)); }
			}
			else
			{
				if (!off)
				{
					audioS = gameObject.AddComponent<AudioSource>();
					for (int i = 0; i < ListOfSounds.Count; i++)
					{
						if (id == ListOfSounds[i].ID)
						{
							audioS.clip = ListOfSounds[i].GetRandomClip();
							audioS.loop = true;
							audioS.pitch = pitch;
							StartCoroutine(LerpSoundUp(audioS, musicVolume));
							ListOfAudioSource.Add(ListOfSounds[i].ID + "Loop", audioS);
							break;
						}
					}
				}
			}
		}
	}
}
[System.Serializable]
public class SoundsItem
{
	public string ID;
	public int minRand, maxRand;
	public bool singular;

	public AudioClip GetRandomClip()
	{
		return LoadFromResources.instance.Load("Sounds/" + ID + (singular ? "" : " (" + Random.Range(minRand, maxRand) + ")")) as AudioClip;
	}
}
