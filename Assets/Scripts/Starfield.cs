﻿using UnityEngine;
using System.Collections;

public class Starfield : MonoBehaviour {
	private Transform thisTransform;
	private ParticleSystem.Particle[] points;

	public int starsMax = 100;
	public float starSize = 1f;
	public float starDistance = 10f;
	public float starClipDistance = 1f;

	private float starDistanceSqr, starClipDistanceSqr;

	private void Start() {
		thisTransform = transform;
		starClipDistanceSqr = Mathf.Pow(starClipDistance, 2f);
		starDistanceSqr = Mathf.Pow(starDistance, 2f);
	}

	private void CreateStars() {
		points = new ParticleSystem.Particle[starsMax];

		for (int i = 0; i < starsMax; i++) {
			points[i].position = Random.insideUnitSphere * starDistance + thisTransform.position;
			points[i].startColor = new Color(1, 1, 1);
			points[i].startSize = starSize;
		}

	}

	void Update() {
		if (points == null) {
			CreateStars();
		}
		
		for (int i = 0; i < starsMax; i++) {
			if ((points[i].position - thisTransform.position).sqrMagnitude > starDistanceSqr) {
				points[i].position = Random.insideUnitSphere.normalized * starDistance + thisTransform.position;
			}

			if ((points[i].position - thisTransform.position).sqrMagnitude <= starClipDistanceSqr) {
				float percent = (points[i].position - thisTransform.position).sqrMagnitude / starClipDistanceSqr;

				points[i].startColor = new Color(1, 1, 1, percent);
				points[i].startSize = percent * starSize;
			}
		}

		GetComponent<ParticleSystem>().SetParticles(points, starsMax);

	}

	/*public int maxStars = 1000;
	public int universeSize = 10;

	private ParticleSystem.Particle[] points;

	private ParticleSystem _particleSystem;

	private void Create() {

		points = new ParticleSystem.Particle[maxStars];

		for (int i = 0; i < maxStars; i++) {
			points[i].position = Random.insideUnitSphere * universeSize;
			points[i].startSize = Random.Range(0.05f, 0.05f);
			points[i].startColor = new Color(1, 1, 1, 1);
		}

		_particleSystem = gameObject.GetComponent<ParticleSystem>();

		_particleSystem.SetParticles(points, points.Length);
	}

	void Start() {
		Create();
	}

	void Update() {
		//You can access the _particleSystem here if you wish
	}*/
}