﻿using UnityEngine;
using System.Collections;

public class KeyboardHotKeys : MonoBehaviour
{
	void Update()
	{
		if (Input.GetKey(KeyCode.Alpha1))
		{
			GridCreator.instance.ButtonClicked(6);
		}
		if (Input.GetKey(KeyCode.Alpha2))
		{
			GridCreator.instance.ButtonClicked(5);
		}
		if (Input.GetKey(KeyCode.Alpha3))
		{
			GridCreator.instance.ButtonClicked(4);
		}
		if (Input.GetKey(KeyCode.Alpha4))
		{
			GridCreator.instance.ButtonClicked(3);
		}
		if (Input.GetKey(KeyCode.Alpha5))
		{
			GridCreator.instance.ButtonClicked(2);
		}
		if (Input.GetKey(KeyCode.Alpha6))
		{
			GridCreator.instance.ButtonClicked(1);
		}
		if (Input.GetKey(KeyCode.Alpha7))
		{
			GridCreator.instance.ButtonClicked(0);
		}
	}
}
