﻿using UnityEngine;
using System.Collections;

public class AstroidRotation : MonoBehaviour {

	float maxSpeed = 1.2f;
	float minSpeed = 0.5f;
	public bool xAxis = true, yAxis = true, zAxis = true;
	float xrot, yrot, zrot;

	void Start() {
		xrot = Random.Range(minSpeed, maxSpeed);
		yrot = Random.Range(minSpeed, maxSpeed);
		zrot = Random.Range(minSpeed, maxSpeed);
	}

	void Update() {
		transform.Rotate(new Vector3((xAxis ? xrot : 0), (yAxis ? yrot : 0), (zAxis ? zrot : 0)));
	}
}
