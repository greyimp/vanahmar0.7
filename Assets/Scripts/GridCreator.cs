﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GridCreator : MonoBehaviour {
	public GameObject BGStars, flyStars, cam;
	public static GridCreator instance;
	public bool canBeReset = true;
	public GameObject intro, ingame, finish, holeHolder;
	public List<Button> buttonsList = new List<Button>();
	float initTime;
	public int Sets = 7;
	public List<Hole> holesList = new List<Hole>();
	public GameObject ObjectPrefab;
	public GameObject[] Metior;
	public float raduis, startingAngle = 0, endAngle = 90;
	public int holesNumber = 10;

	public HoleSets Music;
	public float pitchCOntroller
	{
		get { return commenPitch; }
		set
		{
			commenPitch = value;
			SoundManager.instance.LoopingSound("Orchestra", value);
			Time.timeScale = value;
		}
	}

	List<TimeAstroid> MusicA = new List<TimeAstroid>();
	List<TimeAstroid> MusicAB = new List<TimeAstroid>();
	public int Lives = 3;
	public float startOffSet = 2.5f, commenPitch = 1;
	int currentBeat = 0, _lcLives = 3;
	bool gameOn;

	GameObject Note6;
	Coroutine game;

	void Awake() {
		instance = this;
	}

	void Start() {
		SoundManager.instance.ChangeSoundVolume(0.1f);
		intro.SetActive(true);
		ingame.SetActive(false);
		finish.SetActive(false);
		allah();
		int buttonI = 0;
		float step = (endAngle - startingAngle) / holesNumber;
		float setStep = Mathf.CeilToInt(holesNumber / (Sets * 2));
		for (int i = 0; i < holesNumber; i++) {
			GameObject h = Instantiate(ObjectPrefab, holeHolder.transform) as GameObject;
			h.transform.position = Circle(i * step + startingAngle, raduis);
			h.transform.rotation = Quaternion.LookRotation(h.transform.position - transform.position);
			holesList.Add(h.GetComponent<Hole>());
		}
		step *= holesNumber;
		step /= Sets;
		for (int i = 0; i < Sets; i++) {
			Vector3 position = Circle(startingAngle + step / 2 + i * step, raduis - 1.5f); ;
			position.z -= 2;
			buttonsList[buttonI].transform.position = position;
			//buttonsList[buttonI].transform.rotation = Quaternion.LookRotation(buttonsList[buttonI].transform.position - transform.position);
			//buttonsList[buttonI].transform.localEulerAngles = new Vector3(90,0, 0) + buttonsList[buttonI].transform.localEulerAngles;
			buttonI++;
		}
	}

	public void StartGame() {
		buttonsList[6].gameObject.SetActive(false);
		SoundManager.instance.LoopingSound("Orchestra", 1, true);
		if (game != null) {
			StopCoroutine(game);
			game = null;
		}
		game = StartCoroutine(startMusic());
		ResetButtons();
		ResetHoles();
		ingame.SetActive(true);
		intro.SetActive(false);
		finish.SetActive(false);
		initTime = Time.time;
		currentBeat = 0;
		_lcLives = Lives;
		canBeReset = gameOn = true;
	}

	IEnumerator startMusic() {
		yield return new WaitForSecondsRealtime(startOffSet);
		pitchCOntroller = 0.7f;
	}

	void OnDrawGizmos() {
		float step = (endAngle - startingAngle) / holesNumber;
		for (int i = 0; i < holesNumber; i++) {
			Gizmos.DrawWireSphere(Circle(i * step + startingAngle, raduis), 0.1f);
		}
		float stepangle = (endAngle - startingAngle) / Sets;
		Gizmos.color = Color.yellow;
		for (int i = 0; i < Sets; i++) {
			Gizmos.DrawWireSphere(Circle(startingAngle + stepangle / 2 + i * stepangle, raduis + 30), 0.1f);
		}
	}

	public void ButtonClicked(int id) {
		buttonsList[id].transform.Find("ButtonMesh").GetComponent<Animator>().SetTrigger("Click");
		canBeReset = true;
		ResetHoles();
		SoundManager.instance.OneShotSound("Fire");
		WaveCreator(id);
		for (int i = 0; i < buttonsList.Count; i++) {
			if (id != i)
				buttonsList[i].interactable = false;
		}
	}

	public void ResetButtons() {
		for (int i = 0; i < buttonsList.Count - (Note6 == null ? 1 : 0); i++) {
			buttonsList[i].interactable = true;
			buttonsList[i].transform.Find("ButtonMesh").GetComponent<Animator>().SetTrigger("Off");
			buttonsList[i].transform.Find("ButtonMesh").GetComponent<Animator>().ResetTrigger("Click");
		}
		buttonsList[buttonsList.Count - 1].interactable = false;
	}
	public void ResetHoles() {
		for (int i = 0; i < holesList.Count; i++) {
			holesList[i].Reset();
		}
	}

	void WaveCreator(int id) {
		int MaxHolesEffected = holesNumber / Sets;
		float half = (float)MaxHolesEffected / 2;

		int lowRange = id * MaxHolesEffected, highRange = id * MaxHolesEffected + MaxHolesEffected;
		for (int i = 0; i < holesNumber; i++) {
			if (id == 6) {
				holesList[i].SetValue(1, true);
			} else {
				if (i < highRange && i >= lowRange) {
					float ratio = (((i % half) / (half)) * 0.75f + 0.25f);
					ratio = (i % MaxHolesEffected > half ? 1 - ratio : ratio);
					holesList[i].SetValue(Mathf.Max(0.25f, ratio), true);
				} else {
					if (i < lowRange) {
						holesList[i].SetValue((float)(i) / lowRange * 0.15f + 0.15f, false);
					} else {
						if (i >= highRange) {
							holesList[i].SetValue(0.15f + 0.15f * (1 - (float)(i - highRange) / (holesNumber - highRange)), false);
						}
					}
				}
			}
		}
	}

	void Spawn(TimeAstroid id) {
		float stepangle = (endAngle - startingAngle) / Sets;
		if (id.note == 6) {
			buttonsList[6].transform.Find("ButtonMesh").GetComponent<Animator>().SetTrigger("Attention");
			buttonsList[6].gameObject.SetActive(true);
			ResetButtons();
			for (int i = 0; i < Sets; i++) {
				int metiorSelect = Random.Range(0, 2);
				GameObject g = Instantiate(Metior[metiorSelect], Circle(startingAngle + stepangle / 2 + i * stepangle, raduis + 30), Quaternion.identity, transform) as GameObject;
				g.GetComponent<Astroid>().SetDirection(g.transform.position - transform.position, id);
				Note6 = g;
				TimeAstroid ta = new TimeAstroid(i, 0);
				ta.delta = 1.5f;
				StartCoroutine(ButtonHighLight(ta));
			}
		} else {
			int metiorSelect = Random.Range(0, 2);
			GameObject g = Instantiate(Metior[metiorSelect], Circle(startingAngle + stepangle / 2 + id.note * stepangle, raduis + 30), Quaternion.identity, transform) as GameObject;
			g.GetComponent<Astroid>().SetDirection(g.transform.position - transform.position, id);
		}
		StartCoroutine(ButtonHighLight(id));
	}

	IEnumerator ButtonHighLight(TimeAstroid id) {
		float t = Mathf.Clamp(id.delta, 0.2f, 1f);
		yield return new WaitForSecondsRealtime(2.8f);
		buttonsList[id.note].transform.Find("Image").GetComponent<Image>().color = new Color(0, 1, 0, 0.1f);
		yield return new WaitForSecondsRealtime(t);
		buttonsList[id.note].transform.Find("Image").GetComponent<Image>().color = new Color(1, 1, 1, 0.05f);
		//buttonsList[id].image.color = Color.white;
	}

	Vector3 Circle(float angle, float r) {
		return new Vector3(r * Mathf.Cos(angle * Mathf.Deg2Rad), r * Mathf.Sin(angle * Mathf.Deg2Rad), 0) + transform.position;
	}

	////		.			0  1  2  3  4  5  6
	//	public enum Notes { C, D, F, G, A, B, C2 }
	void allah() {
		#region A
		MusicA.Add(new TimeAstroid(0, 0.944f - 0.944f));
		MusicA.Add(new TimeAstroid(5, 1.040f - 0.944f));
		MusicA.Add(new TimeAstroid(0, 1.154f - 0.944f));
		MusicA.Add(new TimeAstroid(2, 1.760f - 0.944f));

		MusicA.Add(new TimeAstroid(1, 3.008f - 0.944f));
		MusicA.Add(new TimeAstroid(0, 3.098f - 0.944f));
		MusicA.Add(new TimeAstroid(1, 3.269f - 0.944f));
		MusicA.Add(new TimeAstroid(0, 3.536f - 0.944f));
		MusicA.Add(new TimeAstroid(5, 3.797f - 0.944f));

		MusicA.Add(new TimeAstroid(1, 5.146f - 0.944f));
		MusicA.Add(new TimeAstroid(0, 5.253f - 0.944f));
		MusicA.Add(new TimeAstroid(1, 5.346f - 0.944f));
		MusicA.Add(new TimeAstroid(2, 5.928f - 0.944f));

		MusicA.Add(new TimeAstroid(5, 7.229f - 0.944f));
		MusicA.Add(new TimeAstroid(4, 7.23f - 0.944f));
		MusicA.Add(new TimeAstroid(5, 7.472f - 0.944f));
		MusicA.Add(new TimeAstroid(4, 7.765f - 0.944f));

		MusicA.Add(new TimeAstroid(3, 8.008f - 0.944f));
		MusicA.Add(new TimeAstroid(5, 8.240f - 0.944f));
		MusicA.Add(new TimeAstroid(4, 8.512f - 0.944f));
		#endregion

		#region AB
		MusicAB.Add(new TimeAstroid(0, 0.944f - 0.944f));
		MusicAB.Add(new TimeAstroid(5, 1.040f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 1.154f - 0.944f));
		MusicAB.Add(new TimeAstroid(2, 1.760f - 0.944f));

		MusicAB.Add(new TimeAstroid(1, 3.008f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 3.098f - 0.944f));
		MusicAB.Add(new TimeAstroid(1, 3.269f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 3.536f - 0.944f));
		MusicAB.Add(new TimeAstroid(5, 3.797f - 0.944f));

		MusicAB.Add(new TimeAstroid(1, 5.146f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 5.253f - 0.944f));
		MusicAB.Add(new TimeAstroid(1, 5.346f - 0.944f));
		MusicAB.Add(new TimeAstroid(2, 5.928f - 0.944f));

		MusicAB.Add(new TimeAstroid(5, 7.229f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 7.23f - 0.944f));
		MusicAB.Add(new TimeAstroid(5, 7.472f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 7.765f - 0.944f));

		MusicAB.Add(new TimeAstroid(3, 8.008f - 0.944f));
		MusicAB.Add(new TimeAstroid(5, 8.240f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 8.512f - 0.944f));

		MusicAB.Add(new TimeAstroid(3, 9.338f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 9.447f - 0.944f));
		MusicAB.Add(new TimeAstroid(5, 9.6f - 0.944f));

		MusicAB.Add(new TimeAstroid(5, 10.320f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 10.501f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 10.624f - 0.944f));

		MusicAB.Add(new TimeAstroid(5, 10.890f - 0.944f));
		MusicAB.Add(new TimeAstroid(4, 11.152f - 0.944f));
		MusicAB.Add(new TimeAstroid(3, 11.408f - 0.944f));

		MusicAB.Add(new TimeAstroid(2, 11.701f - 0.944f));
		MusicAB.Add(new TimeAstroid(0, 12.157f - 0.944f));
		MusicAB.Add(new TimeAstroid(6, 12.752f - 0.944f));
		#endregion

		Music = new HoleSets();

		//pitch 0.7
		Music.Add(MusicA, 17.664f);
		Music.Add(MusicA, 26.082f);
		Music.Add(MusicA, 34.520f);
		Music.Add(MusicAB, 42.933f);
		//pitch 0.8

		Music.Add(MusicA, 59.754f);
		Music.Add(MusicAB, 68.170f);
		//pitch 0.9

		//Music.Add(MusicA, 127.21f);
		//Music.Add(MusicA, 135.53f);
		Music.Add(MusicA, 177.674f);
		Music.Add(MusicAB, 186.157f);
		//pitch 1

		Music.Add(MusicA, 240.810f);
		Music.Add(MusicA, 266.149f);
		Music.Add(MusicAB, 274.485f);

		Music.Add(MusicA, 291.285f);
		Music.Add(MusicAB, 299.806f);
	}

	public void RemoveHealth() {
		ingame.transform.Find("LivesText").GetComponent<Text>().text = "Lives = " + _lcLives;
		if (_lcLives <= 0)
			LoseGame();
		//else
		//_lcLives--;
	}

	public void LoseGame() {
		SoundManager.instance.LoopingSound("Orchestra", 1, true);
		ingame.SetActive(false);
		finish.SetActive(true);
		finish.transform.Find("WinLose").GetComponent<Text>().text = "YOU LOST :(";
		gameOn = false;
	}

	public void WinGame() {
		ingame.SetActive(false);
		finish.SetActive(true);
		finish.transform.Find("WinLose").GetComponent<Text>().text = "YOU WIN!";
		gameOn = false;
	}
	public float currentTime;
	void FixedUpdate() {
		if (!gameOn) { return; }
		if (currentBeat >= Music.Count) {
			if (transform.childCount == 0)
				WinGame();
			return;
		}
		currentTime = Time.time - initTime;

		if (currentTime > 174 && Time.timeScale != 0.9f) {
			pitchCOntroller = 0.9f;

			cam.GetComponent<CameraShake>().enabled = false;
			flyStars.SetActive(false);
			BGStars.SetActive(true);
		} else {
			if (currentTime < 174 && currentTime > 90 && Time.timeScale != 1) {
				cam.GetComponent<CameraShake>().enabled = true;
				flyStars.SetActive(true);
				BGStars.SetActive(false);
				pitchCOntroller = 1;
			}
		}

		if (currentTime > Music.block[currentBeat].timeSpot) {
			if (currentBeat < Music.Count - 2)
				Music.block[currentBeat].delta = Music.block[currentBeat + 1].timeSpot - Music.block[currentBeat].timeSpot;
			else
				Music.block[currentBeat].delta = 1;
			Spawn(Music.block[currentBeat]);
			currentBeat++;
		}
	}
}
//					0  1  2  3  4  5  6
public enum Notes { A, B, C, D, F, G, C2 }
[System.Serializable]
public class HoleSets {
	public int Count
	{
		get { return block.Count; }
	}
	public float time;
	public List<TimeAstroid> block = new List<TimeAstroid>();
	public HoleSets() {
	}
	public void Add(List<TimeAstroid> l, float t) {
		for (int i = 0; i < l.Count; i++) {
			block.Add(new TimeAstroid(l[i].note, l[i].timeSpot + t));
		}
	}
}

[System.Serializable]
public class TimeAstroid {
	public int note;
	public float timeSpot;
	public float delta;

	public TimeAstroid(int n, float ts) {
		note = n;
		timeSpot = ts;
	}
}