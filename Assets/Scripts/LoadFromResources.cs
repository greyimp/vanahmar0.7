﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class LoadFromResources : MonoBehaviour {
	public static LoadFromResources instance;
	Dictionary<string, Object> resourcesLoaded = new Dictionary<string, Object>();
	void Awake()
	{
		instance = this;
	}
	public Object Load(string path)
	{
		if (resourcesLoaded.ContainsKey(path))
		{
			return resourcesLoaded[path];
		}

		Object loadedItem = Resources.Load(path) as Object;
		resourcesLoaded.Add(path, loadedItem);
		return loadedItem;
	}
}
