﻿using UnityEngine;
using System.Collections;

public class Astroid : MonoBehaviour {
	public TimeAstroid id;
	public float speed;
	Vector3 direction;
	float increment = 0, timer;
	public void SetDirection(Vector3 v, TimeAstroid i) {
		id = i;
		direction = v;
	}

	void OnDestroy() {
	}

	void OnTriggerEnter(Collider other) {
		if (other.transform.tag == "Fire") {
			if (other.transform.GetComponent<Hole>().active && GridCreator.instance.canBeReset) {
				GridCreator.instance.ResetHoles();
				if (id.note != 6)
					GridCreator.instance.canBeReset = false;
				SoundManager.instance.NoteSound(((Notes)id.note).ToString(), id.delta, GridCreator.instance.commenPitch);
				GameObject destroyParticles = Resources.Load("AstroidDestructionParticles") as GameObject;
				destroyParticles.transform.position = this.transform.position;
				Instantiate(destroyParticles);
				destroyParticles.GetComponent<ParticleSystem>().enableEmission = true;
				Destroy(gameObject);
			}
			return;
		}
		if (other.transform.tag == "Earth") {
			//SoundManager.instance.NoteSound(((Notes)id.note).ToString(), id.delta, GridCreator.instance.commenPitch);
			GameObject fireParticles = transform.Find("FireWithSmoke").gameObject;
			fireParticles.SetActive(true);
			Debug.Log(timer);
			other.GetComponent<GridCreator>().RemoveHealth();
			speed = 0.001f;
			increment = 0.0005f;
			foreach (Transform child in transform) {
				child.gameObject.SetActive(true);
			}
			return;
		}
		if (other.transform.tag == "Ground")
		{
			SoundManager.instance.OneShotSound("EarthCollision");
			for (int i = transform.childCount - 1; i >= 0; i--)
			{
				transform.GetChild(i).parent = null;
			}
			Destroy(gameObject);
		}
	}

	void FixedUpdate() {
		//timer += Time.fixedDeltaTime;
		transform.position -= direction * (speed += increment);
	}
}
