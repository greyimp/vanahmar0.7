﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{
	public float speed = 1;
	public bool xAxis, yAxis, zAxis;

	void Update()
	{
		transform.Rotate(new Vector3((xAxis ? 1 : 0), (yAxis ? 1 : 0), (zAxis ? 1 : 0)), speed);
	}
}
