﻿using UnityEngine;
using System.Collections;

public class DeadAstroid : MonoBehaviour
{
	public float speed;
	Vector3 direction;

	public void SetDirection(Vector3 v)
	{
		direction = v;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == "Ground")
		{
			foreach(Transform child in transform)
			{
				child.parent = null;
			}
			Destroy(gameObject);
		}

	}
	void FixedUpdate()
	{
		transform.position -= direction * (speed += 0.0005f);
	}
}
