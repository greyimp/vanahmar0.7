﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {

	float xmin, xmax, ymin, ymax;

	void Start() {
		xmin = transform.position.x - 0.3f;
		xmax = transform.position.x + 0.3f;
		ymin = transform.position.y - 0.3f;
		ymax = transform.position.y + 0.3f;
	}
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (Random.Range(xmin, xmax), Random.Range(ymin, ymax), transform.position.z);

	}
}
